package dtc.mobile.mdss.sampletest

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var mAdapter : AdapterAnswer
    var current_position = 0
    val data = Const.data_soal
    val jawaban_a = Const.listA
    val jawaban_b = Const.listB
    val jawaban_c = Const.listC
    val jawaban_d = Const.listD
    val tempAnswer : MutableList<String> = mutableListOf()
    val tempPos : MutableList<Boolean> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mRecyclerContent = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recyclerView)
        mRecyclerContent.setHasFixedSize(true)
        val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        mRecyclerContent.layoutManager = mLayoutManager
        mRecyclerContent.isNestedScrollingEnabled = true
        mAdapter = AdapterAnswer()
        mRecyclerContent.adapter = mAdapter

        mAdapter.setonActionListener(object : AdapterAnswer.OnAction{
            override fun onClick(benar: Boolean, jawaban: String) {
                tempPos.add(benar)
                tempAnswer.add(jawaban)
                current_position += 1
                nextStep(current_position)
            }
        })
        nextStep(current_position)
        back.setOnClickListener {
            tempAnswer.clear()
            tempPos.clear()
            nextStep(current_position)
            home.visibility = View.VISIBLE
            mHidden.visibility = View.GONE
        }
    }

    fun nextStep(position: Int){
        if(position<5){
            soal.text = data[position]
            var mDataAnswer : MutableList<String> = mutableListOf()
            mDataAnswer.add(jawaban_a[position])
            mDataAnswer.add(jawaban_b[position])
            mDataAnswer.add(jawaban_c[position])
            mDataAnswer.add(jawaban_d[position])
            mAdapter.updateData(mDataAnswer, current_position)
        } else {
            mHidden.visibility = View.VISIBLE
            setViewAnswer(num1, tempPos[0], "1. ${tempAnswer[0]}")
            setViewAnswer(num2, tempPos[1], "2. ${tempAnswer[1]}")
            setViewAnswer(num3, tempPos[2], "3. ${tempAnswer[2]}")
            setViewAnswer(num4, tempPos[3], "4. ${tempAnswer[3]}")
            setViewAnswer(num5, tempPos[4], "5. ${tempAnswer[4]}")
            current_position = 0
        }
    }

    fun setViewAnswer(textView : TextView, answer :Boolean, text : String){
        if(!answer){
            textView.setTextColor(Color.parseColor("#fb4242"))
        } else textView.setTextColor(Color.parseColor("#4CAF50"))
        textView.text = text
        home.visibility = View.GONE
    }
}