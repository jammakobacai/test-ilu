package dtc.mobile.mdss.sampletest

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_answer.view.*

class AdapterAnswer : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var mData : MutableList<String> = mutableListOf()
    private var mOnAction: OnAction? = null
    val truefalse = Const.answer
    var mPros = 0


    interface OnAction {
        fun onClick(benar : Boolean, jawaban : String)
    }


    fun setonActionListener(listener: OnAction) {
        mOnAction = listener
    }

    fun updateData(data : MutableList<String>, mPros : Int){
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
        this.mPros = mPros
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val holder: RecyclerView.ViewHolder
        val view: View
        val inflater = LayoutInflater.from(parent.context)
        view = inflater.inflate(R.layout.item_answer, parent, false)
        holder = ViewHolders(view)
        return  holder
    }

    override fun getItemCount(): Int {
        return  mData.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.data.text = mData[position]
        holder.itemView.setOnClickListener {
            var truef : Boolean
            if(truefalse[mPros] == position)
                truef = true
            else truef = false
            mOnAction?.onClick(truef, mData[position])
        }
    }

    inner class ViewHolders
        (itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

}