package dtc.mobile.mdss.sampletest

object Const {

    val data_soal : List<String> = listOf(
        "10 + 5 = ",
        "siapakah penemu teleskop ?",
        "nama presiden indonesia pertama ?",
        "nama perusahaan yang merilis aplikasi chrome ?",
        "1000 / 5 = ")
    val listA : List<String> = listOf(
        "109",
        "Hans Lippershey",
        "Abdurrahman Wahid",
        "Yahoo",
        "16")
    val listB : List<String> = listOf(
        "15",
        "Abraham Lincoln",
        "Megawati Soekarno Putri",
        "KFC",
        "200")
    val listC : List<String> = listOf(
        "19",
        "Albert Einstein",
        "Soekarno",
        "Xiomai",
        "37")
    val listD : List<String> = listOf(
        "50",
        "Roy Surya",
        "Amien Rais",
        "Google",
        "20")
    val answer : List<Int> = listOf(1, 0, 2, 3, 1)
}